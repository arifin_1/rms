<%@ include file="../layouts/header.jsp" %>
	<div class="mdl-grid box-center">
		<table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
			<thead>
				<tr>
					<th class="mdl-data-table__cell--non-numeric">User Name</th>
                 	<th>Password</th>
                 	<th>
                 		<a class="mdl-button mdl-js-button mdl-button--icon"
                  			href="<%= request.getContextPath() %>/users/create">
				 			<i class="material-icons">add_circle</i>
						</a>
                 	</th>
               	</tr>
			</thead>
			<tbody>
				<% if (request.getAttribute("alert") != null) { %>
					<tr>
						<td colspan="3">
							<b>${alert}! </b>${message}</div>
						</td>
					</tr>
				<% } %>
				
        		<c:forEach items = "${users}" var="user">
                	<tr>
                   		<td class="mdl-data-table__cell--non-numeric"><c:out value = "${user.userName}"/></td>
                   		<td><c:out value = "${user.password}"/></td>
                   		<td>
                   			<a class="mdl-button mdl-js-button mdl-button--icon"
                   				href="<%= request.getContextPath() %>/users/update/${user.id}">
					  			<i class="material-icons">edit</i>
							</a>
							<a class="mdl-button mdl-js-button mdl-button--icon"
								onclick="doDelete('${user.userName}', ${user.id})">
					  			<i class="material-icons">cancel</i>
							</a>
                   		</td>
                 	</tr>
             	</c:forEach>
           	</tbody>
    	</table>
	</div>
<%@ include file="../layouts/footer.jsp" %>
<!-- var = "<%= request.getContextPath() %>/users/delete/${user.id}"; -->
<script type="text/javascript">
	
	function doDelete (name, id) {
		if (confirm("Are you sure want to delete " + name + "'s account?")) {
			window.location.href = "<%= request.getContextPath() %>/users/delete/" + id;
		}
	}
</script>