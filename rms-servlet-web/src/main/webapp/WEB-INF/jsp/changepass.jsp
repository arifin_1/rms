<%@ include file="layouts/header.jsp" %>
	<div class="mdl-grid box-center">
    	<div class="mdl-card mdl-shadow--6dp">
    		<div class="mdl-card__title mdl-color--primary mdl-color-text--white">
				<h2 class="mdl-card__title-text">Acme Co.</h2>
			</div>
			<div class="mdl-card__supporting-text">
				<form id="user-form" method="POST" onsubmit="return checkPass()">
					<% if (request.getAttribute("alert") != null) { %>
						<div class="mdl-components__warning"><b>${alert}! </b>${message}</div>
					<% } %>
					
   					<div class="mdl-textfield mdl-js-textfield">
   						<input type="hidden" name="id" value="${user.id}"/>
   						<input class="mdl-textfield__input" type="text" id="username" name="username" value="Hi, <%= session.getAttribute("userName") %>" readonly/>
   						<label class="mdl-textfield__label" for="username">Username</label>
   					</div>
   					
					<div class="mdl-textfield mdl-js-textfield">
   						<input class="mdl-textfield__input" type="password" id="ouserpass" name="ouserpass" value="${ouserpass}" required/>
   						<label class="mdl-textfield__label" for="ouserpass">Old Password</label>
   					</div>
   					<div class="mdl-textfield mdl-js-textfield">
   						<input class="mdl-textfield__input" type="password" id="nuserpass1" name="nuserpass1" value="${nuserpass1}" required/>
   						<label class="mdl-textfield__label" for="nuserpass1">New Password</label>
   					</div>				
   					<div class="mdl-textfield mdl-js-textfield">
   						<input class="mdl-textfield__input" type="password" id="nuserpass2" name="nuserpass2" value="${nuserpass2}" required/>
   						<label class="mdl-textfield__label" for="nuserpass2">Confirm New Password</label>
   					</div>				
   				</form>
   			</div>
   			<div class="mdl-card__actions mdl-card--border">
   				<button type="submit" form="user-form" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
   					Save
   				</button>
   				<a href="<%= request.getContextPath() %>/users/list" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Cancel</a>
   			</div>
   		</div>
    </div>
<%@ include file="layouts/footer.jsp" %>

<script>
	function checkPass() {
		var pass1 = document.getElementById("nuserpass1").value.trim();
		var pass2 = document.getElementById("nuserpass2").value.trim();
		
		if (pass1 != pass2) {
			alert("Your new passwords do not match");
		}
		
		return pass1 == pass2;
	}
</script>