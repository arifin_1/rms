package com.mitrais.rms.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/AuthenticationFilter")
public class AuthenticationFilter implements Filter {
	private ServletContext context;
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.context = filterConfig.getServletContext();
		this.context.log("AuthenticationFilter initialized");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		
		String[] uri = req.getRequestURI().substring(1).split("/");
		HttpSession session = req.getSession();
    	synchronized(session) {
    		if (uri.length > 1) {
    			if ("users".equals(uri[1]) || ("logout".equals(uri[1])) || ("changepass".equals(uri[1]))) {
    				
					if (session.getAttribute("login") == null) {
						resp.sendRedirect("/" + uri[0] + "/login");
					} else { chain.doFilter(request, response); }
				} else if ("login".equals(uri[1])) {
					if (session.getAttribute("login") != null) {
						resp.sendRedirect("/" + uri[0] + "/users/list");
					} else { chain.doFilter(request, response); }
				} else { chain.doFilter(request, response); }
    		} else {
    			chain.doFilter(request, response);
    		}
    	}		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

}
