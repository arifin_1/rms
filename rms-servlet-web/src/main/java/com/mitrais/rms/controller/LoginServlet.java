package com.mitrais.rms.controller;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mitrais.rms.dao.UserDao;
import com.mitrais.rms.dao.impl.UserDaoImpl;
import com.mitrais.rms.model.User;

import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends AbstractController
{
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
		String path = getTemplatePath(req.getServletPath());
	    RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
	    requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	HttpSession session = req.getSession();
    	synchronized(session) {
	    	String userName = req.getParameter("username");
	    	String password = req.getParameter("userpass");
	    	boolean login = false;
	    	
	    	if (!"".equals(userName) && !"".equals(password)) {
		    	UserDao userDao = UserDaoImpl.getInstance();
		    	User user = userDao.findByUserName(userName).orElse(null);
	
		    	if (user != null) {
		    		if (password.equals(user.getPassword())) {
		    			login = true;
			    		session.setAttribute("login", true);
			    		session.setAttribute("id", user.getId());
			    		session.setAttribute("userName", user.getUserName());
		    		} else { req.setAttribute("alert", "Your password does not match."); }
		    	} else {
		    		req.setAttribute("alert", "Cannot find the username");
		    	}
	    	} else {
	    		req.setAttribute("alert", "Please make sure your username or password does not empty.");
	    	}
	    	
	    	if (login) {
	    		resp.sendRedirect("users/list");
	    	} else {
		    	req.setAttribute("username", userName);
		    	req.setAttribute("userpass", password);
		    	String path = getTemplatePath(req.getServletPath());
		        RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
		        requestDispatcher.forward(req, resp);
	    	}
    	}
    }
}
