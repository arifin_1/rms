package com.mitrais.rms.controller;

import com.mitrais.rms.dao.UserDao;
import com.mitrais.rms.dao.impl.UserDaoImpl;
import com.mitrais.rms.model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.security.KeyStore.Entry;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@WebServlet("/users/*")
public class UserServlet extends AbstractController
{
	private Map url_path;
	private String alert, message;
	private final Map v_path = new HashMap() {{
		put("create", "/form");
		put("update", "/form");
		put("delete", "/list");
	}};
	
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        url_path = convertPathInfo(req.getPathInfo());
        String view_path = getViewPath();
        String path = getTemplatePath(req.getServletPath() + view_path);
        alert = "";
        message = "";
                
        if ("create".equals((String) url_path.get("function"))) {
        	req.setAttribute("create", true);
        } else if ("update".equals((String) url_path.get("function"))) {
        	req.setAttribute("user", doSearchByPath());
        } else if ("delete".equals((String) url_path.get("function"))) {
        	doDeleteByPath();
        }
        
        if (view_path.equals("/list")){
        	UserDao userDao = UserDaoImpl.getInstance();
            List<User> users = userDao.findAll();
            req.setAttribute("users", users);
        }
        
        if (!alert.equals("")) {
        	req.setAttribute("alert", alert);
        	req.setAttribute("message", message);
        }
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
        requestDispatcher.forward(req, resp);
    }
    
    protected String getViewPath() {
    	if (url_path.containsKey("function")) {
    		if (v_path.containsKey(url_path.get("function"))) {
    			return (String) v_path.get(url_path.get("function"));
    		} else { return "/list"; }
    	} else {
    		return "/list";
    	}
    }
    
    protected User doSearchByPath() {
    	boolean found = false;
    	User user = null;
    	if (url_path.containsKey("param-1")) {
    		UserDao userDao = UserDaoImpl.getInstance();
        	Long id = Long.parseLong((String) url_path.get("param-1"));
        	user = userDao.find(id).orElse(null);
        	
        	if (user != null) {
        		found = true;
        	}
    	}
    	
    	if (!found) {
    		alert = "Warning";
    		message = "Could not find the User.";
    	}
    	
    	return user;
    }
    
    protected void doDeleteByPath() {
    	boolean deleted = false;
    	if (url_path.containsKey("param-1")) {
    		UserDao userDao = UserDaoImpl.getInstance();
    		Long id = Long.parseLong((String) url_path.get("param-1"));
    		deleted = userDao.deleteByID(id);
    	}
    	
    	alert = deleted ? "Success" : "Warning";
    	message = deleted ? "The account has been deleted" : "Could not find the User.";
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
    	String alert = "";
		String id = req.getParameter("id").trim();
    	String username = req.getParameter("username").trim();
		String password = replaceIfNull(req.getParameter("userpass"), "");
    	UserDao userDao = UserDaoImpl.getInstance();
    	url_path = convertPathInfo(req.getPathInfo());
    	
    	if ((id.equals("") && !isStringEmpty(username) && !isStringEmpty(password)) ||
    			(!id.equals("") && !isStringEmpty(username))) {
	    	User xUser = userDao.findByUserName(req.getParameter("username")).orElse(null);
	    	if ("create".equals((String) url_path.get("function"))) {
	    		if (xUser == null) {
	    			User newUser = new User(0L, username, password);
	    			userDao.save(newUser);
	    			alert = "Success";
	    			req.setAttribute("message", "The account has been created.");
	    		} else {
	    			alert = "Warning";
	    			req.setAttribute("message", "Username has been taken by other User.");
	    			req.setAttribute("create", true);
	    			req.setAttribute("cuserpass", req.getParameter("cuserpass"));
	    		}
	    	} else {
	    		if (xUser == null) {
		    		xUser = userDao.find(Long.parseLong(req.getParameter("id"))).orElse(null);
		    		if (xUser == null) {
		    			req.setAttribute("alert", "Warning");
		    			req.setAttribute("message", "User account does not exist.");
		    		}
	    		}
	    		
	    		if (xUser.getId() == Long.parseLong(req.getParameter("id"))) {
	    			xUser.setUserName(username);
	    			userDao.update(xUser);
	    			alert = "Success";
	    			req.setAttribute("message", "The account has been updated.");
	    		} else {
	    			alert = "Warning";
	    			req.setAttribute("message", "Username has been taken by other User.");
	    		}
	    	}
	    	
	    	if (alert == "Warning") {
	    		req.setAttribute("user", new User(
	    			Long.parseLong(replaceIfNull(id, "0")),
					req.getParameter("username"),
					req.getParameter("userpass")
	    		));
	    	} else {
	    		List<User> users = userDao.findAll();
	            req.setAttribute("users", users);
	    	}
    	}
	    	
    	req.setAttribute("alert", alert);
    	String path = getTemplatePath(req.getServletPath()+(alert == "Warning" ? "/form" : "/list"));
		RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
        requestDispatcher.forward(req, resp);
    }
}
