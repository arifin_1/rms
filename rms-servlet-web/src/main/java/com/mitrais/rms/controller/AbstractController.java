package com.mitrais.rms.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServlet;

public abstract class AbstractController extends HttpServlet
{
    public static final String VIEW_PREFIX = "/WEB-INF/jsp";
    public static final String VIEW_SUFFIX = ".jsp";

    protected String getTemplatePath(String path)
    {
        if (path.equalsIgnoreCase("/"))
        {
            return VIEW_PREFIX + path + "index" + VIEW_SUFFIX;
        }
        else
        {
            return VIEW_PREFIX + path + VIEW_SUFFIX;
        }
    }
    
    protected Map convertPathInfo(String path)
    {
    	Map ret = new HashMap();
    	String lower_path = path.toLowerCase();
    	String[] array_path = lower_path.substring(1).split("/");
    	
    	ret.put("function", array_path[0]);
    	for (int i=1; i< array_path.length; i++) {
    		ret.put("param-"+i, array_path[i]);
    	}
    	
    	return ret;
    }
    
    protected boolean isStringEmpty(String text) {
    	if ((text == null) || (text.trim().equals(""))) {
    		return true;
    	} else {
    		return false;
    	}
    }
    
    protected String replaceIfNull(String text, String def) {
    	if ((text == null) || ("".equals(text))) {
    		return def;
    	} else {
    		return text.trim();
    	}
    }
}
