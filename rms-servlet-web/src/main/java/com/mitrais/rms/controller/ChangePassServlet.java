package com.mitrais.rms.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mitrais.rms.dao.UserDao;
import com.mitrais.rms.dao.impl.UserDaoImpl;
import com.mitrais.rms.model.User;

@WebServlet("/changepass")
public class ChangePassServlet extends AbstractController {
	private String alert, message;
	
	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String path = getTemplatePath(req.getServletPath());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
        requestDispatcher.forward(req, resp);
    }
	
	@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
		String ouserpass = replaceIfNull(req.getParameter("ouserpass"), "");
		String nuserpass1 = replaceIfNull(req.getParameter("nuserpass1"), "");
		String nuserpass2 = replaceIfNull(req.getParameter("nuserpass2"), "");
		
		HttpSession session = req.getSession();
    	UserDao userDao = UserDaoImpl.getInstance();
		User c_user = userDao.findByUserName(session.getAttribute("userName").toString()).orElse(null);
		
		alert = "";
    	
		if (c_user != null) {
    		if (c_user.getPassword().equals(ouserpass)) {
    			c_user.setPassword(nuserpass1);
    			userDao.update(c_user);
    			req.setAttribute("alert", "Success");
        		req.setAttribute("message", "Your password has been updated.");
    		} else {
    			req.setAttribute("alert", "Warning");
        		req.setAttribute("message", "You old password does not match.");
    		}
    	} else {
    		req.setAttribute("alert", "Warning");
    		req.setAttribute("message", "Cannot found your user data.");
    	}
		
    	String path = getTemplatePath(req.getServletPath());
		RequestDispatcher requestDispatcher = req.getRequestDispatcher(path);
        requestDispatcher.forward(req, resp);
    }
}
